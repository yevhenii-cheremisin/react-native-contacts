import React from "react";
import { TextInput, View, StyleSheet } from "react-native";

const SearchField = (props) => {
  const inputHandler = (text) => {
    props.setText(text);
    props.searchFilter(text);
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder={"Search"}
        placeholderTextColor="#939394"
        value={props.text}
        onChangeText={(text) => inputHandler(text)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    padding: 5,
    marginBottom: 15,
  },
  input: {
    padding: 10,
    paddingHorizontal: 15,
    width: "100%",
    backgroundColor: "#1C1C1E",
    borderWidth: 2,
    borderColor: "#3c3c3e",
    borderRadius: 8,
    color: "#fff",
  },
  searchButton: {
    width: "20%",
    backgroundColor: "#c3c3c5",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8,
    marginLeft: 10,
  },
});

export default SearchField;
