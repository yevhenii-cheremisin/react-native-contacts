import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Alert,
} from "react-native";

import * as Contacts from "expo-contacts";

const ContactAddForm = ({ route, navigation }) => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [PhoneNumber, setPhoneNumber] = useState("");

  const { refreshFunc } = route.params;

  const addContact = async () => {
    if (firstName && PhoneNumber) {
      const newPerson = {
        [Contacts.Fields.FirstName]: firstName,
        [Contacts.Fields.LastName]: lastName,
        [Contacts.Fields.PhoneNumbers]: [
          { label: "mobile", number: PhoneNumber },
        ],
      };
      refreshFunc();
      navigation.navigate("List", {});
      const newContId = await Contacts.addContactAsync(newPerson);
    } else {
      Alert.alert("Enter all fields!");
    }
  };

  const firstNameHandler = (text) => {
    setFirstName(text);
  };

  const lastNameHandler = (text) => {
    setLastName(text);
  };

  const phoneNumberHandler = (text) => {
    setPhoneNumber(text);
  };

  return (
    <View style={styles.container}>
      <View style={styles.block}>
        <Text style={styles.userName}>Add Contact</Text>

        <View style={styles.blockSpace}>
          <Text style={styles.inputInfo}>First Name:</Text>
          <TextInput
            style={styles.inputField}
            title="FirstName"
            onChangeText={firstNameHandler}
            value={firstName}
          />
        </View>

        <View style={styles.blockSpace}>
          <Text style={styles.inputInfoGreen}>Last Name:</Text>
          <TextInput
            style={styles.inputField}
            title="LastName"
            onChangeText={lastNameHandler}
            value={lastName}
          />
        </View>

        <View style={styles.blockSpace}>
          <Text style={styles.inputInfo}>Phone Number:</Text>
          <TextInput
            style={styles.inputField}
            title="PhoneNumber"
            onChangeText={phoneNumberHandler}
            keyboardType="number-pad"
            value={PhoneNumber}
          />
        </View>
        <TouchableOpacity style={styles.addButton} onPress={addContact}>
          <Text style={styles.addButtonText}>ADD</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 30,
    paddingLeft: 10,
    paddingRight: 20,
    backgroundColor: "#000",
    flex: 1,
    height: "100%",
  },
  block: {
    backgroundColor: "#101010",
    padding: 20,
    alignItems: "center",
    borderRadius: 10,
  },
  userName: {
    marginVertical: 40,
    fontSize: 26,
    color: "#fff",
  },
  inputInfo: {
    color: "#fff",
    width: "40%",
    textAlign: "center",
    borderLeftWidth: 1,
    borderColor: "red",
  },
  inputInfoGreen: {
    color: "#fff",
    width: "40%",
    textAlign: "center",
    borderLeftWidth: 1,
    borderColor: "green",
  },
  inputField: {
    padding: 3,
    width: "60%",
    backgroundColor: "#cacaca",
    borderRadius: 5,
  },
  blockSpace: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
  },
  addButton: {
    marginTop: 40,
    backgroundColor: "#2c2c2e",
    padding: 14,
    paddingHorizontal: 26,
    borderRadius: 10,
  },
  addButtonText: {
    color: "#fff",
  },
});

export default ContactAddForm;
