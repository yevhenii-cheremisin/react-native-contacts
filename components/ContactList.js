import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";

import SearchField from "./SearchField";
import UserListField from "./UserListField";

import * as Contacts from "expo-contacts";

export default function ContactList(prop) {
  const [currentContacts, setContacts] = useState([]);
  const [stableContacts, setStableContacts] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const [searchedText, setSearchedText] = useState("");

  async function contactHandler(newContacts) {
    await setContacts(newContacts);
    await setStableContacts(newContacts);
    setIsLoading(false);
  }

  const fetchContacts = async () => {
    (async () => {
      const { status } = await Contacts.requestPermissionsAsync();
      if (status === "granted") {
        const { data } = await Contacts.getContactsAsync({
          fields: [
            Contacts.Fields.PhoneNumbers,
            Contacts.Fields.Emails,
            Contacts.Fields.Image,
          ],
        });

        if (data.length > 0) {
          const contacts = data.map((value) => {
            if (value.phoneNumbers) {
              let user = {};
              user.firstName = value.firstName;
              value.lastName ? (user.lastName = value.lastName) : 0;
              value.emails ? (user.email = value.emails[0].email) : 0;
              user.phone = value.phoneNumbers[0].number.replace(/ /g, "");
              user.id = value.id;
              user.lookupKey = value.lookupKey;
              value.imageAvailable ? (user.image = value.image.uri) : 0;
              return user;
            }
          });
          const filtered = contacts.filter(function (el) {
            return el != null;
          });
          await contactHandler(filtered);
        }
      }
    })();
  };

  useEffect(() => {
    setIsLoading(true);
    fetchContacts();
  }, []);

  const searchFilter = (text) => {
    if (text) {
      const filteredContacts = stableContacts.filter((value) => {
        let fullName = "";
        fullName = value.firstName;
        if (value.lastName) {
          fullName = fullName + " " + value.lastName;
        }
        const searchCont = fullName.toUpperCase();
        const searchText = text.toUpperCase();
        return searchCont.indexOf(searchText) > -1;
      });
      setContacts(filteredContacts);
    } else {
      setContacts(stableContacts);
    }
  };

  const refresh = () => {
    setIsLoading(true);
    fetchContacts();
  };

  return (
    <View style={styles.container}>
      <SearchField
        text={searchedText}
        setText={setSearchedText}
        searchFilter={searchFilter}
      />

      {isLoading ? (
        <View
          style={{
            width: "100%",
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <ActivityIndicator size="large" color="#c3c3c5" />
        </View>
      ) : (
        <FlatList
          onRefresh={() => refresh()}
          refreshing={isLoading}
          data={currentContacts}
          keyExtractor={(user) => user.id}
          style={{ flexGrow: 1 }}
          contentContainerStyle={{ paddingBottom: 50 }}
          renderItem={(userData) => (
            <UserListField
              userInfo={userData}
              navigation={prop.navigation}
              setContacts={setContacts}
              edit={setContacts}
            />
          )}
        />
      )}

      <TouchableOpacity
        style={styles.addContact}
        onPress={() =>
          prop.navigation.navigate("Add", { refreshFunc: refresh })
        }
      >
        <Text style={styles.addContactText}>+</Text>
      </TouchableOpacity>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#000",
    paddingTop: 10,
    paddingHorizontal: 10,
    flex: 1,
    position: "relative",
  },
  addContact: {
    position: "absolute",
    bottom: 20,
    right: 20,
    width: 60,
    height: 60,
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#1c1c1e",
    borderColor: "#ffcc00",
    borderWidth: 2,
  },
  addContactText: {
    fontSize: 25,
    color: "#ffcc00",
  },
});
