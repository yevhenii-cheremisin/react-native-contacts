import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Linking,
  Image,
} from "react-native";

import * as Contacts from "expo-contacts";

const ContactScreen = ({ route, navigation }) => {
  const [styleAvatar, setStyleAvatar] = useState({});
  const { name, color, number, id, email, image, reload } = route.params;

  const getStyle = () => {
    const indStyle = StyleSheet.create({
      ind: {
        width: 80,
        height: 80,
        borderRadius: 40,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: color,
      },
    });
    setStyleAvatar(indStyle);
  };

  useEffect(() => {
    getStyle();
  }, []);

  const callContact = () => {
    Linking.openURL(`tel:${number}`);
  };

  const emailContact = () => {
    Linking.openURL(`mailto:${email}`);
  };

  const chatContact = () => {
    Linking.openURL(`sms:${number}`);
  };

  const deleteContact = async () => {
    await Contacts.removeContactAsync(id);
    navigation.navigate("List", {});
    reload((current) => current.filter((val) => val.id != id));
  };

  return (
    <View style={styles.container}>
      <View style={styles.block}>
        {image ? (
          <Image
            style={styles.img}
            source={{
              uri: image,
            }}
          ></Image>
        ) : (
          <TouchableOpacity style={styleAvatar.ind}>
            <Text style={{ color: "#fff", fontSize: 42 }}>{name[0]}</Text>
          </TouchableOpacity>
        )}

        <Text style={styles.userName}>{name}</Text>
        <Text style={styles.numberShow}>{number}</Text>

        <View style={styles.miniBlocks}>
          <TouchableOpacity style={styles.miniBlock} onPress={emailContact}>
            <Text style={styles.miniBlockText}>Email</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.miniBlock} onPress={chatContact}>
            <Text style={styles.miniBlockText}>Chat</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.deleteButton} onPress={deleteContact}>
            <Text style={styles.miniBlockText}>Delete</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.actionBlock}>
          <TouchableOpacity style={styles.CallButton} onPress={callContact}>
            <Text style={styles.miniBlockText}>Call</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 30,
    paddingLeft: 10,
    paddingRight: 20,
    backgroundColor: "#000",
    flex: 1,
    height: "100%",
  },
  block: {
    backgroundColor: "#101010",
    padding: 20,
    alignItems: "center",
    borderRadius: 10,
  },
  userName: {
    marginTop: 18,
    fontSize: 26,
    color: "#fff",
  },
  numberShow: {
    marginTop: 18,
    fontSize: 16,
    color: "#888",
    textDecorationLine: "underline",
  },
  miniBlocks: {
    marginTop: 40,
    flexDirection: "row",
    justifyContent: "space-around",
    width: "100%",
  },
  miniBlock: {
    borderRadius: 10,
    backgroundColor: "#3c3c3e",
    width: "30%",
    paddingVertical: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  miniBlockText: {
    fontSize: 16,
    color: "#fff",
    padding: 10,
  },
  CallButton: {
    marginTop: 20,
    borderRadius: 10,
    borderColor: "#199",
    backgroundColor: "#3fafaf20",
    borderWidth: 1,
    width: "100%",
    paddingVertical: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  deleteButton: {
    borderRadius: 10,
    borderColor: "#bc3c3e",
    backgroundColor: "#ff3f3f20",
    borderWidth: 1,
    width: "30%",
    paddingVertical: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  actionBlock: {
    marginTop: 60,
    width: "100%",
  },
  img: {
    width: 80,
    height: 80,
    borderRadius: 40,
  },
});

export default ContactScreen;
