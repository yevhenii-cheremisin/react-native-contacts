import React, { useEffect, useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Linking,
  Image,
} from "react-native";

const UserListField = (props) => {
  const [randColor, setRandColor] = useState({});
  const [randColorShare, setRandColorShare] = useState({});

  const colors = [
    "#ff2d55",
    "#5856d6",
    "#ff9500",
    "#ffcc00",
    "#ff3b30",
    "#5ac8fa",
    "#007aff",
    "#4cd964",
    "#888",
  ];

  useEffect(() => {
    getStyle();
  }, []);

  const callContact = () => {
    Linking.openURL(`tel:${props.userInfo.item.phone}`);
  };

  const getStyle = () => {
    let index = props.userInfo.item.id % colors.length;
    const color = colors[index];
    const indStyle = StyleSheet.create({
      ind: {
        width: 40,
        height: 40,
        borderRadius: 20,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: color,
      },
    });
    setRandColorShare(color);
    setRandColor(indStyle);
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.userInfo} onPress={callContact}>
        {props.userInfo.item.image ? (
          <Image
            style={styles.img}
            source={{
              uri: props.userInfo.item.image,
            }}
          ></Image>
        ) : (
          <View style={randColor.ind}>
            <Text style={{ color: "#fff", fontSize: 20 }}>
              {props.userInfo.item.firstName[0]}
            </Text>
          </View>
        )}

        <Text style={styles.userName}>
          {props.userInfo.item.firstName +
            " " +
            (props.userInfo.item.lastName ? props.userInfo.item.lastName : "")}
        </Text>
        <TouchableOpacity
          style={styles.editButton}
          onPress={() =>
            props.navigation.navigate("Edit", {
              name:
                props.userInfo.item.firstName +
                " " +
                (props.userInfo.item.lastName
                  ? props.userInfo.item.lastName
                  : ""),
              color: randColorShare,
              number: props.userInfo.item.phone,
              id: props.userInfo.item.id,
              email: props.userInfo.item.email,
              image: props.userInfo.item.image,
              reload: props.edit,
            })
          }
        >
          <Text style={{ color: "#fff" }}>i</Text>
        </TouchableOpacity>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 12,
    paddingLeft: 10,
    paddingRight: 20,
    backgroundColor: "#1c1c1e",
    borderRadius: 10,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: "#333",
  },
  userInfo: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  userName: {
    color: "#fff",
    width: "70%",
  },
  editButton: {
    padding: 5,
    textAlign: "center",
    borderColor: "#ccc",
    borderWidth: 1,
    borderRadius: 15,
    width: 30,
    height: 30,
    justifyContent: "center",
    alignItems: "center",
  },
  img: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },
});

export default UserListField;
