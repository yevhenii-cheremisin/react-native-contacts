import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet } from "react-native";

import "react-native-gesture-handler";

import ContactList from "./components/ContactList";
import ContactScreen from "./components/ContactScreen";
import ContactAddForm from "./components/ContactAddForm";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer style={styles.container}>
      <Stack.Navigator
        options={{
          cardStyle: {
            backgroundColor: "red",
            opacity: 1,
          },
        }}
      >
        <Stack.Screen
          name="List"
          component={ContactList}
          options={{
            title: "Contacts",
            headerStyle: {
              backgroundColor: "#1c1c1e",
            },
            headerTintColor: "#fff",
          }}
        />
        <Stack.Screen
          name="Edit"
          component={ContactScreen}
          options={{
            title: "More",
            headerStyle: {
              backgroundColor: "#1c1c1e",
            },
            headerTintColor: "#fff",
          }}
        />

        <Stack.Screen
          name="Add"
          component={ContactAddForm}
          options={{
            title: "Add Contact",
            headerStyle: {
              backgroundColor: "#1c1c1e",
            },
            headerTintColor: "#fff",
          }}
        />
      </Stack.Navigator>
      <StatusBar style="auto" />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
